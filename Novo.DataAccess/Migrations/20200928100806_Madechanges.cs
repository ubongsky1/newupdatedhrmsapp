﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class Madechanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RemainingLeaves",
                table: "LeaveUploads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TotalLeaves",
                table: "LeaveUploads",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UtilizedLeaves",
                table: "LeaveUploads",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RemainingLeaves",
                table: "LeaveUploads");

            migrationBuilder.DropColumn(
                name: "TotalLeaves",
                table: "LeaveUploads");

            migrationBuilder.DropColumn(
                name: "UtilizedLeaves",
                table: "LeaveUploads");
        }
    }
}
