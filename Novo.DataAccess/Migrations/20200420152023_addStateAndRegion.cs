﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class addStateAndRegion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Region",
                table: "EmployeeContacts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "States",
                table: "EmployeeContacts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Region",
                table: "EmployeeContacts");

            migrationBuilder.DropColumn(
                name: "States",
                table: "EmployeeContacts");
        }
    }
}
