﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class addBeneficiary : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isHrDisapprove",
                table: "Leaves",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isUnitHeadDisapprove",
                table: "Leaves",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "BeneficiaryAddress",
                table: "EmployeeContacts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "BeneficiaryNo",
                table: "EmployeeContacts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmergencyAddress",
                table: "EmployeeContacts",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "EmergencyNo",
                table: "EmployeeContacts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isHrDisapprove",
                table: "Leaves");

            migrationBuilder.DropColumn(
                name: "isUnitHeadDisapprove",
                table: "Leaves");

            migrationBuilder.DropColumn(
                name: "BeneficiaryAddress",
                table: "EmployeeContacts");

            migrationBuilder.DropColumn(
                name: "BeneficiaryNo",
                table: "EmployeeContacts");

            migrationBuilder.DropColumn(
                name: "EmergencyAddress",
                table: "EmployeeContacts");

            migrationBuilder.DropColumn(
                name: "EmergencyNo",
                table: "EmployeeContacts");
        }
    }
}
