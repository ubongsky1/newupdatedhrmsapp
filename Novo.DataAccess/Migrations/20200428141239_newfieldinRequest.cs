﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class newfieldinRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isAdmApproved",
                table: "Requests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isHrApproved",
                table: "Requests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isHrDisapprove",
                table: "Requests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isUnitHeadApproved",
                table: "Requests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "isUnitHeadDisapprove",
                table: "Requests",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isAdmApproved",
                table: "Requests");

            migrationBuilder.DropColumn(
                name: "isHrApproved",
                table: "Requests");

            migrationBuilder.DropColumn(
                name: "isHrDisapprove",
                table: "Requests");

            migrationBuilder.DropColumn(
                name: "isUnitHeadApproved",
                table: "Requests");

            migrationBuilder.DropColumn(
                name: "isUnitHeadDisapprove",
                table: "Requests");
        }
    }
}
