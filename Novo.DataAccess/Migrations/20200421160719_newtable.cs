﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class newtable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
         
            migrationBuilder.CreateTable(
                name: "NewTasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AddedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    TaskName = table.Column<string>(nullable: true),
                    TaskType = table.Column<string>(nullable: true),
                    TaskDescription = table.Column<string>(nullable: true),
                    AssignBy = table.Column<string>(nullable: true),
                    EmployeeId = table.Column<int>(nullable: false),
                    ExpectedStartDate = table.Column<string>(nullable: true),
                    ExpectedEndDate = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    Remark = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    isUnitHeadApproved = table.Column<bool>(nullable: false),
                    isHrApproved = table.Column<bool>(nullable: false),
                    isEmpApproved = table.Column<bool>(nullable: false),
                    isUnitHeadDisapprove = table.Column<bool>(nullable: false),
                    isHrDisapprove = table.Column<bool>(nullable: false),
                    AssignToId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewTasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NewTasks_Employee_AssignToId",
                        column: x => x.AssignToId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Requests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AddedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    RequestName = table.Column<string>(nullable: true),
                    RequestDescription = table.Column<string>(nullable: true),
                    AssignToId = table.Column<int>(nullable: false),
                    Comment = table.Column<string>(nullable: true),
                    EmployeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Requests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Requests_Employee_AssignToId",
                        column: x => x.AssignToId,
                        principalTable: "Employee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NewTasks_AssignToId",
                table: "NewTasks",
                column: "AssignToId");

            migrationBuilder.CreateIndex(
                name: "IX_Requests_AssignToId",
                table: "Requests",
                column: "AssignToId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NewTasks");

            migrationBuilder.DropTable(
                name: "Requests");

            migrationBuilder.DropColumn(
                name: "isEmpApproved",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "isHrApproved",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "isHrDisapprove",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "isUnitHeadApproved",
                table: "Tasks");

            migrationBuilder.DropColumn(
                name: "isUnitHeadDisapprove",
                table: "Tasks");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_AssignToId",
                table: "Tasks",
                column: "AssignToId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tasks_Employee_AssignToId",
                table: "Tasks",
                column: "AssignToId",
                principalTable: "Employee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
