﻿using Novo.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NovoApp.Moddel
{
    public class FormsViewModel
    {
        public Form form { get; set; }
        public Appraisal appraisalForm { get; set; }
    }
}
