#pragma checksum "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d586dc1cd4b37cfda68c4f4dd11e59813faa7a21"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_EmploymentDetails1_Index), @"mvc.1.0.view", @"/Views/EmploymentDetails1/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/EmploymentDetails1/Index.cshtml", typeof(AspNetCore.Views_EmploymentDetails1_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\_ViewImports.cshtml"
using NovoApp;

#line default
#line hidden
#line 2 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\_ViewImports.cshtml"
using NovoApp.Moddel;

#line default
#line hidden
#line 3 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\_ViewImports.cshtml"
using Novo.Models.Entity;

#line default
#line hidden
#line 4 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\_ViewImports.cshtml"
using Microsoft.AspNetCore.Identity;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d586dc1cd4b37cfda68c4f4dd11e59813faa7a21", @"/Views/EmploymentDetails1/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2c88feaf465e480a3f60b1fca67186b9ca2f0114", @"/Views/_ViewImports.cshtml")]
    public class Views_EmploymentDetails1_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<Novo.Models.Entity.EmploymentDetails>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Details", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Delete", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(58, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 3 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
            BeginContext(101, 29, true);
            WriteLiteral("\r\n<h2>Index</h2>\r\n\r\n<p>\r\n    ");
            EndContext();
            BeginContext(130, 37, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "cbdbb03538204214873640765ce6605b", async() => {
                BeginContext(153, 10, true);
                WriteLiteral("Create New");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(167, 92, true);
            WriteLiteral("\r\n</p>\r\n<table class=\"table\">\r\n    <thead>\r\n        <tr>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(260, 46, false);
#line 16 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.EmployeeId));

#line default
#line hidden
            EndContext();
            BeginContext(306, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(362, 44, false);
#line 19 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Fullname));

#line default
#line hidden
            EndContext();
            BeginContext(406, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(462, 46, false);
#line 22 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.BloodGroup));

#line default
#line hidden
            EndContext();
            BeginContext(508, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(564, 39, false);
#line 25 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.DOB));

#line default
#line hidden
            EndContext();
            BeginContext(603, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(659, 42, false);
#line 28 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Gender));

#line default
#line hidden
            EndContext();
            BeginContext(701, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(757, 49, false);
#line 31 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.MaritalStatus));

#line default
#line hidden
            EndContext();
            BeginContext(806, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(862, 47, false);
#line 34 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Designation));

#line default
#line hidden
            EndContext();
            BeginContext(909, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(965, 46, false);
#line 37 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.EmployeeNo));

#line default
#line hidden
            EndContext();
            BeginContext(1011, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(1067, 41, false);
#line 40 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Email));

#line default
#line hidden
            EndContext();
            BeginContext(1108, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(1164, 52, false);
#line 43 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.ModeOfEmployment));

#line default
#line hidden
            EndContext();
            BeginContext(1216, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(1272, 44, false);
#line 46 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.JobTitle));

#line default
#line hidden
            EndContext();
            BeginContext(1316, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(1372, 46, false);
#line 49 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.EntryLevel));

#line default
#line hidden
            EndContext();
            BeginContext(1418, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(1474, 46, false);
#line 52 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.IsUnitHead));

#line default
#line hidden
            EndContext();
            BeginContext(1520, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(1576, 46, false);
#line 55 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.IsDeptHead));

#line default
#line hidden
            EndContext();
            BeginContext(1622, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(1678, 50, false);
#line 58 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.IsRegionalHead));

#line default
#line hidden
            EndContext();
            BeginContext(1728, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(1784, 48, false);
#line 61 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.CurrentLevel));

#line default
#line hidden
            EndContext();
            BeginContext(1832, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(1888, 49, false);
#line 64 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.LastPromotion));

#line default
#line hidden
            EndContext();
            BeginContext(1937, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(1993, 52, false);
#line 67 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.DateOfEmployment));

#line default
#line hidden
            EndContext();
            BeginContext(2045, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(2101, 49, false);
#line 70 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.DateOfLeaving));

#line default
#line hidden
            EndContext();
            BeginContext(2150, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(2206, 53, false);
#line 73 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.YearsOfExperience));

#line default
#line hidden
            EndContext();
            BeginContext(2259, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(2315, 41, false);
#line 76 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Image));

#line default
#line hidden
            EndContext();
            BeginContext(2356, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(2412, 39, false);
#line 79 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.CUG));

#line default
#line hidden
            EndContext();
            BeginContext(2451, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(2507, 43, false);
#line 82 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.AddedBy));

#line default
#line hidden
            EndContext();
            BeginContext(2550, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(2606, 45, false);
#line 85 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.UpdatedBy));

#line default
#line hidden
            EndContext();
            BeginContext(2651, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(2707, 46, false);
#line 88 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.CreateDate));

#line default
#line hidden
            EndContext();
            BeginContext(2753, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(2809, 46, false);
#line 91 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.UpdateDate));

#line default
#line hidden
            EndContext();
            BeginContext(2855, 55, true);
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
            EndContext();
            BeginContext(2911, 44, false);
#line 94 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.IsDelete));

#line default
#line hidden
            EndContext();
            BeginContext(2955, 86, true);
            WriteLiteral("\r\n            </th>\r\n            <th></th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
            EndContext();
#line 100 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
 foreach (var item in Model) {

#line default
#line hidden
            BeginContext(3073, 48, true);
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(3122, 45, false);
#line 103 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.EmployeeId));

#line default
#line hidden
            EndContext();
            BeginContext(3167, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(3223, 43, false);
#line 106 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Fullname));

#line default
#line hidden
            EndContext();
            BeginContext(3266, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(3322, 45, false);
#line 109 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.BloodGroup));

#line default
#line hidden
            EndContext();
            BeginContext(3367, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(3423, 38, false);
#line 112 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.DOB));

#line default
#line hidden
            EndContext();
            BeginContext(3461, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(3517, 41, false);
#line 115 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Gender));

#line default
#line hidden
            EndContext();
            BeginContext(3558, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(3614, 48, false);
#line 118 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.MaritalStatus));

#line default
#line hidden
            EndContext();
            BeginContext(3662, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(3718, 46, false);
#line 121 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Designation));

#line default
#line hidden
            EndContext();
            BeginContext(3764, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(3820, 45, false);
#line 124 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.EmployeeNo));

#line default
#line hidden
            EndContext();
            BeginContext(3865, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(3921, 40, false);
#line 127 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Email));

#line default
#line hidden
            EndContext();
            BeginContext(3961, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(4017, 51, false);
#line 130 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.ModeOfEmployment));

#line default
#line hidden
            EndContext();
            BeginContext(4068, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(4124, 43, false);
#line 133 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.JobTitle));

#line default
#line hidden
            EndContext();
            BeginContext(4167, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(4223, 45, false);
#line 136 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.EntryLevel));

#line default
#line hidden
            EndContext();
            BeginContext(4268, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(4324, 45, false);
#line 139 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.IsUnitHead));

#line default
#line hidden
            EndContext();
            BeginContext(4369, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(4425, 45, false);
#line 142 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.IsDeptHead));

#line default
#line hidden
            EndContext();
            BeginContext(4470, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(4526, 49, false);
#line 145 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.IsRegionalHead));

#line default
#line hidden
            EndContext();
            BeginContext(4575, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(4631, 47, false);
#line 148 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.CurrentLevel));

#line default
#line hidden
            EndContext();
            BeginContext(4678, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(4734, 48, false);
#line 151 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.LastPromotion));

#line default
#line hidden
            EndContext();
            BeginContext(4782, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(4838, 51, false);
#line 154 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.DateOfEmployment));

#line default
#line hidden
            EndContext();
            BeginContext(4889, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(4945, 48, false);
#line 157 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.DateOfLeaving));

#line default
#line hidden
            EndContext();
            BeginContext(4993, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(5049, 52, false);
#line 160 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.YearsOfExperience));

#line default
#line hidden
            EndContext();
            BeginContext(5101, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(5157, 40, false);
#line 163 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Image));

#line default
#line hidden
            EndContext();
            BeginContext(5197, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(5253, 38, false);
#line 166 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.CUG));

#line default
#line hidden
            EndContext();
            BeginContext(5291, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(5347, 42, false);
#line 169 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.AddedBy));

#line default
#line hidden
            EndContext();
            BeginContext(5389, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(5445, 44, false);
#line 172 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.UpdatedBy));

#line default
#line hidden
            EndContext();
            BeginContext(5489, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(5545, 45, false);
#line 175 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.CreateDate));

#line default
#line hidden
            EndContext();
            BeginContext(5590, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(5646, 45, false);
#line 178 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.UpdateDate));

#line default
#line hidden
            EndContext();
            BeginContext(5691, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(5747, 43, false);
#line 181 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.IsDelete));

#line default
#line hidden
            EndContext();
            BeginContext(5790, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(5845, 53, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "98c48bf4d29645e6a84b0ed92f25ce29", async() => {
                BeginContext(5890, 4, true);
                WriteLiteral("Edit");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 184 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
                                       WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5898, 20, true);
            WriteLiteral(" |\r\n                ");
            EndContext();
            BeginContext(5918, 59, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "daaa3cf849914ecca5a36f936c2c110a", async() => {
                BeginContext(5966, 7, true);
                WriteLiteral("Details");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 185 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
                                          WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(5977, 20, true);
            WriteLiteral(" |\r\n                ");
            EndContext();
            BeginContext(5997, 57, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "d3e3743ba4434ebbaad8ececb5552f8c", async() => {
                BeginContext(6044, 6, true);
                WriteLiteral("Delete");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 186 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
                                         WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(6054, 36, true);
            WriteLiteral("\r\n            </td>\r\n        </tr>\r\n");
            EndContext();
#line 189 "C:\Users\novoadmin\Desktop\hrms\NovoApp\Views\EmploymentDetails1\Index.cshtml"
}

#line default
#line hidden
            BeginContext(6093, 24, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<Novo.Models.Entity.EmploymentDetails>> Html { get; private set; }
    }
}
#pragma warning restore 1591
