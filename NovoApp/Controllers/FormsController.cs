﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NovoApp.Controllers
{
    public class FormsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public FormsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }
        // GET: /<controller>/
        public IActionResult Staffs()
        {
            var users = _userManager.Users;
            return View(users);
        }

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int id, [Bind("Question1,Question2,Question3,Question4,Question5,Question6,Question7,Question8,Question9,Question10,Question11,Question12,Question13,Question14,Question15,EmployeeId,Weight1,Weight2,Weight3,Weight4,Weight5,Weight6,Weight7,Weight8,Weight9,Weight10,Weight11,Weight12,Weight13,Weight14,Weight15")] Form form)
        {
            if (ModelState.IsValid)
            {
                _context.Forms.Add(form);
                await _context.SaveChangesAsync();

                form.EmployeeId = id;
                
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Staffs));
            }   
            return View();
        }


        public IActionResult Details(int? id)
        {
            var from = _context.Forms.Where(x => x.EmployeeId == id).Select(a => a.Id).FirstOrDefault();
            var use = _context.Forms.Where(x => x.EmployeeId == id).FirstOrDefault();
            
            if (use == null)
            {
                return NotFound();
            }
            var forms = _context.Forms.FirstOrDefault(x => x.Id == from);
            if (forms == null)
            {
                return NotFound();
            }
            return View(forms);
        }

        //// GET: AppraisalForms/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    var user = await _userManager.GetUserAsync(User);
        //    var from = _context.Forms.Where(x => x.EmployeeId == id).FirstOrDefault();

        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var forms = await _context.Forms.FindAsync(from);
        //    if (forms == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewBag.id = _context.Forms.Where(x => x.EmployeeId == id).Select(a => a.Id).FirstOrDefault();
        //    return View(forms);
        //}

        //public async Task<IActionResult> Edit(int? id)
        //{
        //    var user = await _userManager.GetUserAsync(User);
        //    var from = _context.Forms.Where(x => x.EmployeeId == id).Select(a => a.Id).FirstOrDefault();

        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var forms = await _context.Forms.FindAsync(from);
        //    if (forms == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewBag.id = _context.Forms.Where(x => x.EmployeeId == id).Select(a => a.Id).FirstOrDefault();
        //    return View(forms);
        //}

        public async Task<IActionResult> Edit(int? id)
        {
            var user = await _userManager.GetUserAsync(User);
            var from = _context.Forms.Where(x => x.EmployeeId == id).Select(a => a.Id).FirstOrDefault();

            if (id == null)
            {
                return NotFound();
            }

            var forms = await _context.Forms.FindAsync(from);
            if (forms == null)
            {
                return NotFound();
            }
            ViewBag.id = _context.Forms.Where(x => x.EmployeeId == id).Select(a => a.Id).FirstOrDefault();
            return View(forms);
        }

        //public async Task<IActionResult> Edit()
        //{
        //    var user = await _userManager.GetUserAsync(User);
        //    var from = _context.Forms.Where(x => x.EmployeeId == user.Id).Select(a => a.Id).FirstOrDefault();

        //    //if (id == null)
        //    //{
        //    //    return NotFound();
        //    //}

        //    var forms = await _context.Forms.FindAsync(from);
        //    if (forms == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewBag.id = _context.Forms.Where(x => x.EmployeeId == user.Id).Select(a => a.Id).FirstOrDefault();
        //    return View(forms);
        //}


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Question1,Question2,Question3,Question4,Question5,Question6,Question7,Question8,Question9,Question10,Question11,Question12,Question13,Question14,Question15,EmployeeId,Weight1,Weight2,Weight3,Weight4,Weight5,Weight6,Weight7,Weight8,Weight9,Weight10,Weight11,Weight12,Weight13,Weight14,Weight15")] Form form)
        {
            var from = _context.Forms.Where(x => x.EmployeeId == id).Select(a => a.Id).FirstOrDefault();
            var use = _context.Forms.Find(from);
            //if (id != form.EmployeeId)
            if (id != form.EmployeeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    //_context.Update(form);
                    //await _context.SaveChangesAsync();
                    use.EmployeeId = id;
                    use.Id = from;
                    use.Question1 = form.Question1;
                    use.Question2 = form.Question2;
                    use.Question3 = form.Question3;
                    use.Question4 = form.Question4;
                    use.Question5 = form.Question5;
                    use.Question6 = form.Question6;
                    use.Question7 = form.Question7;
                    use.Question8 = form.Question8;
                    use.Question9 = form.Question9;
                    use.Question10 = form.Question10;
                    use.Question11 = form.Question11;
                    use.Question12 = form.Question12;
                    use.Question13 = form.Question13;
                    use.Question14 = form.Question14;
                    use.Question15 = form.Question15;
                    use.Weight1 = form.Weight1;
                    use.Weight2 = form.Weight2;
                    use.Weight3 = form.Weight3;
                    use.Weight4 = form.Weight4;
                    use.Weight5 = form.Weight5;
                    use.Weight6 = form.Weight6;
                    use.Weight7 = form.Weight7;
                    use.Weight8 = form.Weight8;
                    use.Weight9 = form.Weight9;
                    use.Weight10 = form.Weight10;
                    use.Weight11 = form.Weight11;
                    use.Weight12 = form.Weight12;
                    use.Weight13 = form.Weight13;
                    use.Weight14 = form.Weight14;
                    use.Weight15 = form.Weight15;
                    
                   
                    
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FormExists(form.EmployeeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Staffs));
            }
            return View(form);
        }

        private bool FormExists(int id)
        {
            return _context.Forms.Any(e => e.Id == id);
        }
    }
}
