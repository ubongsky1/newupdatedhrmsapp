﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;

namespace NovoApp.Controllers
{
    public class LgasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LgasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Lgas
        public async Task<IActionResult> Index()
        {
            return View(await _context.Lga.ToListAsync());
        }

        // GET: Lgas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lga = await _context.Lga
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lga == null)
            {
                return NotFound();
            }

            return View(lga);
        }

        // GET: Lgas/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Lgas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] Lga lga)
        {
            if (ModelState.IsValid)
            {
                _context.Add(lga);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(lga);
        }

        // GET: Lgas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lga = await _context.Lga.FindAsync(id);
            if (lga == null)
            {
                return NotFound();
            }
            return View(lga);
        }

        // POST: Lgas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Name,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] Lga lga)
        {
            if (id != lga.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(lga);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LgaExists(lga.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(lga);
        }

        // GET: Lgas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lga = await _context.Lga
                .FirstOrDefaultAsync(m => m.Id == id);
            if (lga == null)
            {
                return NotFound();
            }

            return View(lga);
        }

        // POST: Lgas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var lga = await _context.Lga.FindAsync(id);
            _context.Lga.Remove(lga);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LgaExists(int id)
        {
            return _context.Lga.Any(e => e.Id == id);
        }
    }
}
