﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;

namespace NovoApp.Controllers
{
    public class NewTasksController : Controller
    {
        private readonly ApplicationDbContext _context;

        public NewTasksController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: NewTasks
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.NewTasks.Include(n => n.ApplicationUser);
            return View(await applicationDbContext.ToListAsync());
        }

        public async Task<IActionResult> TAskHR()
        {
            var applicationDbContext = _context.NewTasks.Include(n => n.ApplicationUser);
            return View(await applicationDbContext.ToListAsync());
        }

        public async Task<IActionResult> TAskUHead()
        {
            var applicationDbContext = _context.NewTasks.Include(n => n.ApplicationUser);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: NewTasks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newTasks = await _context.NewTasks
                .Include(n => n.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (newTasks == null)
            {
                return NotFound();
            }

            return View(newTasks);
        }

        // GET: NewTasks/Create
        public IActionResult Create()
        {
            ViewData["AssignToId"] = new SelectList(_context.Users, "Id", "UserName");
            return View();
        }

        // POST: NewTasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TaskName,TaskType,TaskDescription,AssignBy,EmployeeId,ExpectedStartDate,ExpectedEndDate,StartDate,EndDate,Comment,Remark,Status,isUnitHeadApproved,isHrApproved,isEmpApproved,isUnitHeadDisapprove,isHrDisapprove,AssignToId,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] NewTasks newTasks)
        {
            if (ModelState.IsValid)
            {
                _context.Add(newTasks);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AssignToId"] = new SelectList(_context.Users, "Id", "FirstName", newTasks.AssignToId);
            return View(newTasks);
        }

        // GET: NewTasks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newTasks = await _context.NewTasks.FindAsync(id);
            if (newTasks == null)
            {
                return NotFound();
            }
            ViewData["AssignToId"] = new SelectList(_context.Users, "Id", "FirstName", newTasks.AssignToId);
            return View(newTasks);
        }

        // POST: NewTasks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TaskName,TaskType,TaskDescription,AssignBy,EmployeeId,ExpectedStartDate,ExpectedEndDate,StartDate,EndDate,Comment,Remark,Status,isUnitHeadApproved,isHrApproved,isEmpApproved,isUnitHeadDisapprove,isHrDisapprove,AssignToId,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] NewTasks newTasks)
        {
            if (id != newTasks.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(newTasks);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NewTasksExists(newTasks.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AssignToId"] = new SelectList(_context.Users, "Id", "FirstName", newTasks.AssignToId);
            return View(newTasks);
        }

        // GET: NewTasks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var newTasks = await _context.NewTasks
                .Include(n => n.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (newTasks == null)
            {
                return NotFound();
            }

            return View(newTasks);
        }

        // POST: NewTasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var newTasks = await _context.NewTasks.FindAsync(id);
            _context.NewTasks.Remove(newTasks);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NewTasksExists(int id)
        {
            return _context.NewTasks.Any(e => e.Id == id);
        }
    }
}
