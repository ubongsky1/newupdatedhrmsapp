﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;

namespace NovoApp.Controllers
{
    public class LeaveHandoversController : Controller
    {
        private readonly ApplicationDbContext _context;

        public LeaveHandoversController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: LeaveHandovers
        public async Task<IActionResult> Index()
        {
            return View(await _context.LeaveHandovers.ToListAsync());
        }

        // GET: LeaveHandovers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaveHandover = await _context.LeaveHandovers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (leaveHandover == null)
            {
                return NotFound();
            }

            return View(leaveHandover);
        }

        // GET: LeaveHandovers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LeaveHandovers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LeaveId,EmployeeId,Employee,Note,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] LeaveHandover leaveHandover)
        {
            if (ModelState.IsValid)
            {
                _context.Add(leaveHandover);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(leaveHandover);
        }

        // GET: LeaveHandovers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaveHandover = await _context.LeaveHandovers.FindAsync(id);
            if (leaveHandover == null)
            {
                return NotFound();
            }
            return View(leaveHandover);
        }

        // POST: LeaveHandovers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("LeaveId,EmployeeId,Employee,Note,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] LeaveHandover leaveHandover)
        {
            if (id != leaveHandover.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(leaveHandover);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LeaveHandoverExists(leaveHandover.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(leaveHandover);
        }

        // GET: LeaveHandovers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaveHandover = await _context.LeaveHandovers
                .FirstOrDefaultAsync(m => m.Id == id);
            if (leaveHandover == null)
            {
                return NotFound();
            }

            return View(leaveHandover);
        }

        // POST: LeaveHandovers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var leaveHandover = await _context.LeaveHandovers.FindAsync(id);
            _context.LeaveHandovers.Remove(leaveHandover);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LeaveHandoverExists(int id)
        {
            return _context.LeaveHandovers.Any(e => e.Id == id);
        }
    }
}
