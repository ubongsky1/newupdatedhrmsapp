﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Novo.Models.Entity
{
    [Table("Months")]
    public class Month : Base
    {
     
        public string Name { get; set; }
        public bool IsDeleted { get; set; }

    }
}
