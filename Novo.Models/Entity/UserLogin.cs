﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Novo.Models.Entity
{
    public class UserLogin : IdentityUserLogin<int>
    {
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
