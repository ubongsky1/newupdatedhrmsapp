﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Novo.Models.Entity
{
    public class Appraise : Base
    {
       
        [Display(Name = "Employee")]
        public int EmployeeId { get; set; }

       
        [Display(Name = "Date Of Appraisal")]
        [DataType(DataType.Date)]
        public string AppraisalDate { get; set; }

       
        [Display(Name = "Head Of Unit Remarks")]
        public string HeadOfUnitRemark { get; set; }

       
        [Display(Name = "HR Remarks")]
        public string HrRemark { get; set; }

        
        [Display(Name = "Grade")]
        public string Grade { get; set; }
       
        [Display(Name = "Appraisal Mode")]
        public string AppraisalMode { get; set; }

        
        [Display(Name = "Department")]
        public Departments Departments { get; set; }

        public Status Status { get; set; }

       
        [Display(Name = "Rating By Employee")]
        public int EmployeeRating { get; set; }

       
        [Display(Name = "Rating By Manager")]
        public int ManagerRating { get; set; }

      
        [Display(Name = "Rating By HR")]
        public int HrRating { get; set; }
    }
}
