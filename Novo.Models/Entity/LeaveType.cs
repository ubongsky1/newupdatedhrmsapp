﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novo.Models.Entity
{
   public class LeaveType : Base
    {
        [Display(Name = "Leave Type")]
        public string Type { get; set; }

        [Display(Name = "Leave Type Description")]
        public string Description { get; set; }
    }
}
