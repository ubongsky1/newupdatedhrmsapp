﻿using Microsoft.AspNetCore.Http;
using Novo.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novo.Models.ViewModels
{
    public class QualificationEditVm: Base
    {
        [Display(Name = "Employee Id")]
        public int EmployeeId { get; set; }


        [Required]
        [Display(Name = "Academic Qualification")]
        public string Academic { get; set; }
        [Required]
        [Display(Name = "Academic Qualification Description")]
        [DataType(DataType.MultilineText)]
        public string AcademicDescription { get; set; }

        [Display(Name = "Professional Qualification")]
        public string Professional { get; set; }

        [Display(Name = "Professional Qualification Description")]
        [DataType(DataType.MultilineText)]
        public string ProfessionalDescription { get; set; }

        
        [Display(Name = "Institution Attended")]
        [DataType(DataType.MultilineText)]
        public string Institution { get; set; }


        [Required]
        [Display(Name = "Duration")]
        public int Duration { get; set; }

        [Required]
        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        public string StartDate { get; set; }

        [Required]
        [Display(Name = "Date Of Completion")]
        [DataType(DataType.Date)]
        public string EndDate { get; set; }
       
        [Display(Name = "Certificate")]
        public IFormFile Image { get; set; }
        public string ExistingPhotoPath { get; set; }
    }
}
